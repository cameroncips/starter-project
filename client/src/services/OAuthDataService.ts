import axios from 'axios';

class OAuthDataService {
  login() {
    return axios.get('http://localhost:8080/auth/google', {
        withCredentials: true
    });
  }
}

export default new OAuthDataService();
