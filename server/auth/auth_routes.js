const router = require('express').Router();
const passport = require('./passport-setup');

router.get('/google', passport.authenticate('google', {
    scope: ['profile', 'email']
}));

router.get('/google/callback', passport.authenticate('google', { 
    failureRedirect: '/api/error',
    successRedirect: '/sprint3'
}),
    // Handle Success
    // (req, res) => {
    //     return res.json({ success: true, message: 'Successfully logged in with google!'});
    // },
)

module.exports = router;